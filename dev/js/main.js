/* frameworks */
//=include ../../node_modules/jquery/dist/jquery.min.js

/* libs */
//=include lib/flatpickr.min.js
//=include lib/modernizr-custom.js

/* plugins */
//=include ../../node_modules/jquery-validation/dist/jquery.validate.min.js
//=include ../../node_modules/progressbar.js/dist/progressbar.js
//=include ../../node_modules/svg4everybody/dist/svg4everybody.min.js

/* separate */
//=include helpers/object-fit.js
//=include helpers/valid.js

/* components */
//=include components/header.js
//=include components/datepicker.js
//=include components/js-progressbar.js
//=include components/employment-menu.js

// the main code
svg4everybody();