(function () {
    if ($('.js-datepicker').length) {
        $('.js-datepicker').each(function() {
            $(this).flatpickr({
                disableMobile: 'true'
            });
        })
    }
}());