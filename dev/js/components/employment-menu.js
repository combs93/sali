(function () {
    $('.js-sublist-trigger').on('click', function() {
        const $this = $(this);
        const $parent = $this.closest('.employment-menu__box');
        const $dataMenu = $parent.data('menu');

        
        $this.closest('.employment-aside').find('.employment-menu__box.is-opened').not('[data-menu="'+$dataMenu+'"]').find('.employment-menu__sublist').slideUp(300);
        $this.closest('.employment-aside').find('.employment-menu__box.is-opened').not('[data-menu="'+$dataMenu+'"]').removeClass('is-opened');

        $this.closest('.employment-aside').find('.employment-menu__sublist[data-sublist="'+$dataMenu+'"]').slideToggle(300);
        $this.closest('.employment-aside').find('.employment-menu__box[data-menu="'+$dataMenu+'"]').toggleClass('is-opened');
    });
    
    $('.js-submenu-trigger').on('click', function() {
        $(this).siblings('.employment-menu__submenu').slideToggle(300);
        $(this).closest('.employment-menu__sublist__item').toggleClass('is-opened');
    });




    // employment form
    $('.page-form__add-employment').on('submit', function() {
        if ($(this).find('.form-control').val().length < 1) {
            return;
        }

        $('.page-form__employment-group').addClass('is-inited is-collapsed');
        $('.page-form__employment-group').find('.page-form__employment-group__inner').hide();

        const clonedGroup = $('.page-form').find('.page-form__employment-group').first().clone().removeClass('is-collapsed');
        clonedGroup.find('.page-form__employment-group__inner').attr('style', '');
        clonedGroup.insertBefore($('.page-form__submit'));
        
        $('.page-form__employment-group').each(function(i,item) {
            const groupIndex = i;
            $(this).find('input').each(function() {
                if ($(this).attr('id')) {
                    $(this).attr('id', $(this).attr('id') + groupIndex)
                }
                if ($(this).attr('name')) {
                    $(this).attr('name', $(this).attr('name') + groupIndex)
                }
            });
            $(this).find('.form-control-radio-label').each(function() {
                if ($(this).attr('for')) {
                    $(this).attr('for', $(this).attr('for') + groupIndex)
                }
            });
        });
    });

    $('.employment-section').on('click', '.page-form__employment-group__toggler', function() {
        $(this).closest('.page-form__employment-group__actions').siblings('.page-form__employment-group__inner').slideToggle(600);
        $(this).closest('.page-form__employment-group').toggleClass('is-collapsed');
    });

    $('.employment-section').on('click', '.page-form__employment-group__remove', function() {
        $(this).closest('.page-form__employment-group').remove();
    });
}());