(function () {
    $('.js-nav-trigger').on('click', function() {
        $('#header_navigation_menu').toggleClass('nav-open')
    });

    $('.header__nav__item--sub').on('click', function() {
        if ($(window).width() < 992 ) {
            $(this).toggleClass('is-active');
            $(this).find('.header__sublist').slideToggle(300);
        }
    });

    $('.header__sublist__item').on('click', function(e) {
        if ($(window).width() < 992 ) {
            e.stopPropagation();
        }
    });
}());