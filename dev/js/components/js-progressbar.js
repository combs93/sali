(function () {
    // mein page big progress
    if ($('#complete_block_chart_bar').length) {
        const $triggerInput = $('#complete_block_chart_trigger');

        let $percentage = parseFloat($triggerInput.val());
        let $animatedNum = $percentage / 100;
        
        $triggerInput.on('change', function() {
            $percentage = parseFloat($(this).val());
            $animatedNum = $percentage / 100;
            
            $bar.animate($animatedNum);
        });
    
        const $bar = new ProgressBar.Circle('#complete_block_chart_bar', {
            strokeWidth: 12,
            easing: 'easeInOut',
            duration: 1500,
            color: '#396FAB',
            trailColor: '#F0F8FF',
            trailWidth: 12,
            svgStyle: null
        });
        
        $bar.animate($animatedNum);


        // CHANGE PROGRESS VALUE!!
        $('.btn.btn--filled.btn--xs').on('click', function() {
            $triggerInput.val(80).trigger('change');
            $('#complete_block_chart_data').text(80);
        });
    }


    // main menu progress
    if ($('.employment-menu').length) {
        $('.employment-menu__item__chart').each(function(i, item) {
            let $this = $(this);
            let $inputTrigger = $this.siblings('input');

            let $progressVal = parseFloat($inputTrigger.val());
            let $animatedVal = $progressVal / 100;

            let $menuBar = new ProgressBar.Circle(this, {
                strokeWidth: 5,
                easing: 'easeInOut',
                duration: 1500,
                color: '#428BCA',
                trailColor: '#E8E8E8',
                trailWidth: 5,
                svgStyle: null
            });

            // trigger animate          
            $menuBar.animate($animatedVal);

        });
    }
}());